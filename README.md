# polygen_bot #

`polygen_bot` is a Telgram bot which replies to `/gen` and some other commands by looking up a [polygen](http://www.polygen.org/) grammar (by name or description), executing it and returning the result.

It runs just fine on Linux with *polygen* installed (I'm hosting it on an Ubuntu 14.04 server).

The bot uses the [telepot](https://github.com/nickoala/telepot) library to connect with Telegram; it expects it to be already installed (use `pip install telepot`) and available for use.

## Try it! ##

From Telegram, start a conversation with *@polygen_bot* (or add it to a group).

## License ##

Standard 3-clause BSD (see LICENSE file).