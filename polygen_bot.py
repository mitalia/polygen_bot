#!/usr/bin/env python2
import sys
import telepot
import random
from telepot.delegate import per_chat_id, create_open
from polygen_interface import *
import re
import threading
from collections import defaultdict
import json
import signal
import atexit

BOTNAME = "polygen_bot"

class StatsHandler:
    def __init__(self):
        self.lock = threading.RLock()
        self.load({})

    def grammar_served(self, grammar):
        with self.lock:
            self.grammar_counts[grammar]+=1

    def good_command_received(self, command):
        with self.lock:
            self.good_commands_counts[command]+=1

    def bad_command_received(self):
        with self.lock:
            self.bad_commands_count+=1

    def dump(self):
        with self.lock:
            return {
                'grammar_counts': self.grammar_counts,
                'good_commands_counts': self.good_commands_counts,
                'bad_commands_count': self.bad_commands_count
                }

    def load(self, d):
        with self.lock:
            self.grammar_counts = defaultdict(int, d.get('grammar_counts', {}))
            self.good_commands_counts = defaultdict(int, d.get('good_commands_counts', {}))
            self.bad_commands_count = d.get('bad_commands_count', 0)

stats = StatsHandler()

class PolygenResponder(telepot.helper.ChatHandler):
    command_re = re.compile("/(?P<command>[a-z_0-9]*)([@](?P<botname>[a-z_0-9]*))?", re.IGNORECASE)
    def __init__(self, seed_tuple, timeout):
        super(PolygenResponder, self).__init__(seed_tuple, timeout)
        self.last_match = None

    def on_message(self, msg):
        send = self.sender.sendMessage
        outbuf = [""]
        outbuf_mode = None
        send_max = 1000
        def partial_send(s):
            def dosend():
                send(outbuf[0], parse_mode=outbuf_mode)
                outbuf[0]=""
            if s is None:
                if len(outbuf[0]):
                    dosend()
            else:
                if len(outbuf[0]) + len(s) > send_max:
                    dosend()
                    while len(s)>send_max:
                        outbuf[0] = s[:send_max]
                        dosend()
                        s = s[send_max:]
                outbuf[0] += s

        txt = msg.get('text', '').strip()
        if not txt.startswith('/'):
            return
        m = self.command_re.match(txt)
        if not m:
            return
        if (m.groupdict()["botname"] or BOTNAME)!=BOTNAME:
            return
        cmd = m.groupdict()["command"].lower()
        cmdline = txt[m.end():].strip()
        goodcmd = True

        if cmd=='gen':
            if len(cmdline)==0:
                send("You have to specify the topic! Type /gen followed by the topic name or by some words that may match the topic description; use /list to get a list of all available topics")
            else:
                print cmdline, "=>",
                match = catalog.quick_match(cmdline)
                print match
                if match:
                    self.last_match = match
                    stats.grammar_served(match)
                    send(poly_exec(match))
        elif cmd=='again':
            if self.last_match:
                match = self.last_match
                print "(again) =>", match
                stats.grammar_served(match)
                send(poly_exec(match))
            else:
                send("Oops, I forgot the last one! Try again with /gen")
        elif cmd=='list':
            outbuf_mode = "Markdown"
            partial_send("*Polygen topics list*\n\n")
            for v in sorted(catalog.grammars.itervalues(), key=lambda v: v['shortname']):
                sn = v['shortname']
                partial_send("*%s*: %s\n" % (sn, v.get('title', '').replace('*', '\\*').replace('_', '\_')))
        elif cmd=='start' or cmd=='startgroup':
            nice_grammars = ["beghelli", "ristoranti cinesi", "delonghi", "farmaci", "cavallo goloso", "unieuro", "cani pericolosi", "film d'azione", "dux", "verdena", "teen", "teen2", "bofh", "comuni lombardi", "ghezzi"]
            send("""
*polygen_bot* bridges the gap between Telegram and Polygen

_Try it out!_
/gen %s

Use /list to discover all topics""" % (random.choice(nice_grammars)),parse_mode='Markdown')
        elif cmd=='stats':
            outbuf_mode = "Markdown"
            st = stats.dump()
            partial_send("*Grammars served*\n")
            def topdown(d):
                return sorted(d.iteritems(), key=lambda x: -x[1])
            tot = 0
            for k,v in topdown(st['grammar_counts']):
                g = catalog.grammars[k]
                gtitle = g['shortname']
                partial_send("_%s_: %d\n" % (gtitle.replace("_", "\_"), v))
                tot += v
            partial_send("---\n_Total_: %d\n" % (tot))
            partial_send("*Good commands listened*\n")
            tot = 0
            for k,v in topdown(st['good_commands_counts']):
                partial_send("_%s_: %d\n" % (k, v))
                tot += v
            partial_send("---\n_Total_: %d\n" % (tot))
            partial_send("*Bad commands ignored*: %d\n" % st['bad_commands_count'])
        elif cmd=='rawstats':
            send(json.dumps(stats.dump()))
        elif cmd=='help':
            send("""
*polygen_bot - a Telegram - Polygen bridge*

Use /gen _text_ to generate something matching _text_; _text_ may either be the topic name or a few words matching the topic description

Use /list to get a list of all available topic (with description)

Use /again to generate again using the last topic

Use /stats to obtain some statistics about the bot""", parse_mode='Markdown')
        else:
            goodcmd = False
        partial_send(None)
        if goodcmd:
            stats.good_command_received(cmd)
        else:
            stats.bad_command_received()

TOKEN = sys.argv[1]  # get token from command-line
STATS_FILE = ""
if len(sys.argv) > 2:
    STATS_FILE = sys.argv[2]
print "Loading Polygen Catalog..."
catalog = GrammarsCatalog()
print "Done!"
if len(STATS_FILE):
    print "Loading saved stats..."
    try:
        with open(STATS_FILE, "rb") as fd:
            stats.load(json.load(fd))
    except:
        print "Couldn't load saved stats"
    def cleanup():
        print "Saving stats..."
        with open(STATS_FILE, "wb") as fd:
            json.dump(stats.dump(), fd)
    atexit.register(cleanup)
    signal.signal(signal.SIGTERM, lambda *args, **kwargs: sys.exit(1))

bot = telepot.DelegatorBot(TOKEN, [
    (per_chat_id(), create_open(PolygenResponder, timeout=180)),
])
print "We are up!"
bot.notifyOnMessage(run_forever=True)
