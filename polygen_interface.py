#!/usr/bin/env python2
import os
import subprocess
import searchengine

GRAMMAR_PATHS = ["/usr/share/polygen/"]

def poly_exec(*args):
    return subprocess.check_output(["polygen"] + list(args))

_bag = searchengine.baggize

class GrammarsCatalog:
    def __init__(self, grammar_paths = GRAMMAR_PATHS):
        self.grammar_paths = grammar_paths
        self.grammars = {}
        self.index = searchengine.Index()
        self.shortnames = {}
        for gp in grammar_paths:
            for dirpath, _, files in os.walk(gp):
                for f in files:
                    if f.lower().endswith(".grm"):
                        self.add_grammar(os.path.join(dirpath, f))

    def add_grammar(self, full_path):
        raw_info = poly_exec("-info", full_path)
        short_name = os.path.basename(full_path).lower()[:-4]
        info = {'shortname': short_name}
        for l in raw_info.split('\n'):
            l = str(l).strip()
            colon = l.find(':')
            if colon==-1: continue
            info[l[:colon].strip()] = l[colon+1:].strip()
        self.grammars[full_path] = info
        self.shortnames[short_name] = full_path
        self.index.addBag(full_path, [short_name] + _bag(info.get('title', '') + " " + info.get('topic','')))

    def quick_match(self, text):
        shortname = str(text.strip().lower())
        if shortname in self.shortnames:
            return self.shortnames[shortname]
        matches = self.index.getMatches(_bag(text))
        if len(matches)==0:
            return None
        return max(matches.items(), key=lambda x: x[1])[0]

if __name__ == "__main__":
    print "Loading grammars..."
    catalog = GrammarsCatalog()
    print "Done!"
    while True:
        match = catalog.quick_match(raw_input())
        if match:
            print poly_exec(match)
        else:
            print '=('
